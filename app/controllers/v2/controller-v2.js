export let renderListFood = (list) => {
  let content = "";
  const formatter = new Intl.NumberFormat("vn-VN", {
    style: "currency",
    currency: "VND",
  });
  list.forEach((food) => {
    content += `<tr>
        <td>${food.id}</td>
        <td>${food.ten}</td>
        <td>${food.loai ? "Mặn" : "Chay"}</td>
        <td>${formatter.format(food.gia)}</td>
        <td>${food.khuyenMai}</td>
        <td>0</td>
        <td>${food.tinhTrang ? "Còn" : "Hết"}</td> 
        <td>
        <button onclick="suaMonAn(${
          food.id
        })" class="btn btn-warning">Sửa</button>
        <button onclick="xoaMonAn(${
          food.id
        })" class="btn btn-danger">Xóa</button>
        </td>   
        </tr>`;
  });
  document.getElementById("tbodyFood").innerHTML = content;
};
