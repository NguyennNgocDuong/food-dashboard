export function layThongTinTuForm() {
    const foodID = document.getElementById('foodID').value;
    const tenMon = document.getElementById('tenMon').value;
    const loai = document.getElementById('loai').value !== 'loai1' ? true : false;
    const giaMon = document.getElementById('giaMon').value;
    const khuyenMai = document.getElementById('khuyenMai').value;
    const tinhTrang = document.getElementById('tinhTrang').value !== '0' ? true : false;
    const hinhMon = document.getElementById('hinhMon').value;
    const moTa = document.getElementById('moTa').value;

    return {
        foodID,
        tenMon,
        loai,
        giaMon,
        khuyenMai,
        tinhTrang,
        hinhMon,
        moTa
    };
}

export function showThongTinLenForm(monAn) {
    // cu phap es6 destructuring obj
    let { foodID, tenMon, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa } = monAn

    document.getElementById('spMa').innerText = foodID;
    document.getElementById('spTenMon').innerText = tenMon;
    document.getElementById('spLoaiMon').innerText = loai == 'loai1' ? 'Chay' : 'Mặn';
    document.getElementById('spGia').innerText = giaMon;
    document.getElementById('spKM').innerText = `${khuyenMai}%`;
    document.getElementById('spTT').innerText = tinhTrang == 0 ? 'Hết' : 'Còn';
    document.getElementById('imgMonAn').src = hinhMon;
    document.getElementById('pMoTa').innerText = moTa;
    document.getElementById('spGiaKM').innerText = monAn.tinhGiaKhuyenMai();
}