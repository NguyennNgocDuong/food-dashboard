export class MonAnV2 {
  constructor(ten, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa, id) {
    this.ten = ten;
    this.loai = loai;
    this.gia = gia;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhMon = hinhMon;
    this.moTa = moTa;
    this.id = id;
  }
}
