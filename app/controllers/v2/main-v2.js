import { layThongTinTuForm } from "../v1/controller-v1.js";
import { renderListFood } from "./controller-v2.js";
import { MonAnV2 } from "../../models/v2/food-model-v2.js";

const BASE_URL = "https://62f8b754e0564480352bf3cc.mockapi.io";

let renderTableService = () => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      renderListFood(res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
renderTableService();

// document.getElementById("btnThemMon").addEventListener("click", function () {
//   let a = layThongTinTuForm();
//   console.log("a: ", a);
// });
window.themMon = themMon;
function themMon() {
  let data = layThongTinTuForm();

  let monAn = new MonAnV2(
    data.tenMon,
    data.loai,
    data.giaMon,
    data.khuyenMai,
    data.tinhTrang,
    data.hinhMon,
    data.moTa,
    data.foodID
  );
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then((res) => {
      renderTableService();
    })

    .catch((err) => {
      console.log("err: ", err);
    });
}

/**
 * truthy
 *
 *
 * falsy: undefined,null,false,NaN,0,""
 *
 *
 */
window.suaMonAn = suaMonAn;
window.xoaMonAn = xoaMonAn;
function suaMonAn(id) {
  console.log(id);
}

function xoaMonAn(id) {
  axios({
    url: `${BASE_URL}/food/id`,
  });
}
